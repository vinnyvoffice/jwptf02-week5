<%@taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<title>movies jsp</title>
</head>
<body>
	<div class="container">
		<h1 class="pagetitle">movies jsp</h1>


		<form:form  method="post" modelAttribute="movie" action="/j/movies">
			<div id="messageArea"></div>
			<form:hidden path="id"/>
			<fieldset class="form-group">
				<form:label path="title" for="movie.field.title">Title: </form:label>
				<form:input path="title" cssClass="form-control" id="movie.field.title"/>
				<span id="movie.help.title"><form:errors path="title" cssClass="alert-danger"/></span>
			</fieldset>
			<fieldset class="form-group">
				<label for="movie.field.releasedDate">Released Date: </label>
				<form:input path="releasedDate" type="date" cssClass="form-control" id="movie.field.releasedDate" />
				<span id="movie.help.releasedDate"><form:errors path="releasedDate" cssClass="alert-danger"/></span>
			</fieldset>
			<fieldset class="form-group">
				<label for="movie.field.budget">Budget: </label>
				<form:input path="budget" cssClass="form-control" id="movie.field.budget"/>
				<span id="movie.help.budget"><form:errors path="budget" cssClass="alert-danger"/></span>
			</fieldset>
			<fieldset class="form-group">
				<label>Poster: </label>
				<form:input path="poster" cssClass="form-control" id="movie.field.poster"/>
				<span id="movie.help.poster"><form:errors path="poster" cssClass="alert-danger"/></span>
			</fieldset>
			<fieldset class="form-group">
				<label>Rating: </label>
				<form:input path="rating" cssClass="form-control" id="movie.field.rating"/>
				 <span id="movie.help.rating"><form:errors path="rating" cssClass="alert-danger"/></span>
			</fieldset>
			<fieldset class="form-group">
				<label>Category: </label>
				<form:select path="category" id="movie.field.category" cssClass="form-control">
					<form:option value="Best Picture">Best Picture</form:option>
					<option value="Cinematography">Cinematography</option>
					<option value="Costume Design">Costume Design</option>
					<option value="Directing">Directing</option>
					<option value="Film Editing">Film Editing</option>
					<option value="Markup and Hairstyling">Markup and
						Hairstyling</option>
					<option value="Music Original Score">Music (Original
						Score)</option>
					<option value="Music Original Song">Music (Original Song)</option>
					<option value="Production Design">Production Design</option>
					<option value="Sound Editing">Sound Editing</option>
					<option value="Sound Mixing">Sound Mixing</option>
					<option value="Visual Effects">Visual Effects</option>
					<option value="Writing Adapted Screenplay">Writing
						(Adapted Screenplay)</option>
					<option value="Writing Original Screenplay">Writing
						(Original Screenplay)</option>
				</form:select> <span id="movie.help.category"></span>
			</fieldset>
			<fieldset>
				<label>Result: </label>
				<form:radiobutton path="result" value="true" label="Winner"   id="movie.field.result.winner" />
				<form:radiobutton path="result" value="false" label="Nominee"  id="movie.field.result.nominee" />
			</fieldset>
			<fieldset>
				<input id="btnSave" class="btn btn-primary" type="submit"
					value="Save" /> <input id="btnClear" class="btn btn-secondary"
					type="submit" value="Clear" />
			</fieldset>
		</form:form>
		<table class="table">
			<caption id="tblcaption">
				<span>${moviesCounter}</span> items
			</caption>
			<thead>
				<tr>
					<th>title</th>
					<th>released date</th>
					<th>budget</th>
					<th>poster</th>
					<th>rating</th>
					<th>category</th>
					<th>winner</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody id="tblBody">
				<c:forEach var="movie" items="${movies}">
					<tr>
						<td>${movie.title}</td>
						<td>${movie.releasedDate}</td>
						<td>${movie.budget}</td>
						<td><a
							href="${movie.poster}"
							target="_black"><img
								src="${movie.poster}"
								width="30px"></a></td>
						<td>${movie.rating}</td>
						<td>${movie.category}</td>
						<td>${movie.result}</td>
						<td><form method="post" action="/j/movies/actions/delete/${movie.id}">
								<button  type="submit">delete</button>
							</form></td>
						<td><form method="post" action="/j/movies/actions/edit/${movie.id}">
								<button type="submit">edit</button>
							</form></td>
					</tr>
				</c:forEach>
			<tbody>
		</table>
	</div>

	<div id="chartContainer" style="height: 300px; width: 100%;"></div>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

	<script>
	  	var data = [];
	  	<c:forEach var="item" items="${data}">
		  	data.push({ y: ${item.counter}, label: ${item.year}});
	  	</c:forEach>
		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled : true,
			theme : "light2",
			title : {
				text : "Movies by Year"
			},
			axisY : {
				title : "quantidade"
			},
			data : [ {
				type : "column",
				showInLegend : true,
				legendMarkerColor : "grey",
				legendText : "Year",
				dataPoints : data
			} ]
		});
		chart.render();
	</script>


	<script src="app.js">

	</script>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>

</body>
</html>
