package br.com.voffice.java.jwptf02.week5.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.voffice.java.jwptf02.week5.entities.MovieCharacter;

@Repository
public interface DataJpaMovieCharacterRepository extends JpaRepository<MovieCharacter, Long> {}