package br.com.voffice.java.jwptf02.week5.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.voffice.java.jwptf02.week5.entities.Movie;

@Controller
public class DemoThymeleafController {

	@GetMapping("/demo")
	public String findOne(Model model) {
		Movie movie =  Movie.builder()
				.title("Meu filme")
				.build();
		model.addAttribute("outra", movie);
		return "movie";
	}
}
