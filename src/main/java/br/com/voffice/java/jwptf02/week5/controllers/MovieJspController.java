package br.com.voffice.java.jwptf02.week5.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.voffice.java.jwptf02.week5.entities.Movie;
import br.com.voffice.java.jwptf02.week5.repositories.jpa.DataJpaMovieRepository;

@Controller
public class MovieJspController {

	@Autowired
	DataJpaMovieRepository movieRepository;

	@GetMapping("/pages/j/movies")
	public String openPage() {
		return "redirect:/j/movies";
	}

	@GetMapping("/j/movies")
	public String getMovies(Model model) {
		List<Movie> movies = movieRepository.findAll();
		model.addAttribute("movie", Movie.builder().title(null).releasedDate(LocalDate.now()).build());
		model.addAttribute("movies", movies);
		model.addAttribute("moviesCounter", movieRepository.count());
		model.addAttribute("data", movieRepository.findAllGroupByYear());
		return "movies/list";
	}

	@PostMapping("/j/movies")
	public String postMovie(@Valid Movie movie, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "movies/list";
		}
		movieRepository.save(movie);
		return "redirect:/j/movies";
	}

	@PostMapping("/j/movies/actions/edit/{id}")
	public String editMovie(Model model, @PathVariable Long id) {
		Optional<Movie> optional = movieRepository.findById(id);
		List<Movie> movies = movieRepository.findAll();
		model.addAttribute("movie", optional.orElse(null));
		model.addAttribute("movies", movies);
		model.addAttribute("moviesCounter", movieRepository.count());
		model.addAttribute("data", movieRepository.findAllGroupByYear());
		return "movies/list";
	}

	@PostMapping("/j/movies/actions/delete/{id}")
	public String deleteMovie(Model model, @PathVariable Long id) {
		movieRepository.deleteById(id);
		return "redirect:/j/movies";
	}


}
