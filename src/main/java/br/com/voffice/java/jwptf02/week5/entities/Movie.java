package br.com.voffice.java.jwptf02.week5.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="movies")
@Data
@Builder
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude="characters")
public  class Movie {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Size(min=3)
	private String title;
	@NotNull
	@Column(name = "released_date")
	@DateTimeFormat(iso=ISO.DATE)
	private LocalDate releasedDate;
	@DecimalMin(value="0",message="{validations.min}")
	private Double budget;
	private String poster;
	@Min(value=0)
	@Max(100)
	private Integer rating;
	private String category;
	private Boolean result;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="movie")
	private List<MovieCharacter> characters;

	public void addCharacter(MovieCharacter character) {
		this.getCharacters().add(character);
	}

}
