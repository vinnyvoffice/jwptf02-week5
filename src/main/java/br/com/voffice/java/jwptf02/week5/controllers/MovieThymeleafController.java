package br.com.voffice.java.jwptf02.week5.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.voffice.java.jwptf02.week5.entities.Movie;
import br.com.voffice.java.jwptf02.week5.repositories.jpa.DataJpaMovieRepository;

@Controller
public class MovieThymeleafController {

	@Autowired
	DataJpaMovieRepository movieRepository;

	@GetMapping("/pages/t/movies")
	public String openPage() {
		return "redirect:/t/movies";
	}

	@GetMapping("/t/movies")
	public String getMovies(Model model) {
		List<Movie> movies = movieRepository.findAll();
		model.addAttribute("movie", Movie.builder().title(null).releasedDate(LocalDate.now()).build());
		model.addAttribute("movies", movies);
		model.addAttribute("moviesCounter", movieRepository.count());
		model.addAttribute("data", movieRepository.findAllGroupByYear());
		return "thymeleaf/movies/list";
	}

	@GetMapping("/pages/t/search-movie")
	public String openSearchMovie() {
		return "thymeleaf/movies/search-movie";
	}

	@PostMapping("/t/movies")
	public String postMovie(@Valid Movie movie, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "thymeleaf/movies/list";
		}
		movieRepository.save(movie);
		return "redirect:/t/movies";
	}


	@PostMapping("/t/search-movie")
	public String searchMovie(Model model, String title, @RequestParam("year") Integer year) {
		List<Movie> movies = movieRepository.findByTitleAndYear(title, year);
		model.addAttribute("movies", movies);
		model.addAttribute("moviesCounter", movies.size());
		return "thymeleaf/movies/search-movie";
	}

	@PostMapping("/t/movies/actions/edit/{id}")
	public String editMovie(Model model, @PathVariable Long id) {
		Optional<Movie> optional = movieRepository.findById(id);
		List<Movie> movies = movieRepository.findAll();
		model.addAttribute("movie", optional.orElse(null));
		model.addAttribute("movies", movies);
		model.addAttribute("moviesCounter", movieRepository.count());
		return "thymeleaf/movies/list";
	}

	@PostMapping("/t/movies/actions/delete/{id}")
	public String deleteMovie(Model model, @PathVariable Long id) {
		movieRepository.deleteById(id);
		return "redirect:/t/movies";
	}


}
