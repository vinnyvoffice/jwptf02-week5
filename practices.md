# Práticas

* O projeto de referência deve ser clonado do  endereço `https://gitlab.com/vinnyvoffice/jwptf02-week5.git`
* O projeto das práticas deve ser criado localmente
* O projeto das práticas deve configurado para usar o seu repositório remoto no gitlab ou github
* O projeto das práticas deve ser configurado para usar o heroku como ambiente de entrega
* A codificação deve ser feita usando Eclipse JEE ou outra IDE java com suporte a Tomcat 9, Java 8 e Maven 3
* A entrega de cada prática deve ser feita na respectiva atividade do Google Classroom informando a url da tag no repositório remoto git mais o endereço da aplicação no heroku


## prática 17 (51)

* modificando filmes com spring web jsp

### Itens

*  5101 criando um filme
*  5102 consultando todos os filmes
*  5103 consultando um filme a partir do id
*  5104 removendo um filme
*  5105 alterando um filme
*  5106 apresentando mensagens de validação ao lado do campo

## prática 18 (52)

* consultando filmes com spring web jsp

### Itens

*  5201 consultando um filme a partir do título e do ano de lançamento
*  5202 consultando os filmes de um mesmo ano de lançamento
*  5203 consultando todos os filmes classificados ascendentemente por título
*  5204 apresentando o total de filmes no rodapé da lista
*  5205 apresentando gráfico de barras da quantidade de filmes por ano
*  5206 apresentando os rótulos de campo com conteúdo de arquivo de propriedades (i18n)

## prática 19 (53)

* modificando filmes com spring web thymeleaf

### Itens

*  5301 criando um filme
*  5302 consultando todos os filmes
*  5303 consultando um filme a partir do id
*  5304 removendo um filme
*  5305 alterando um filme
*  5306 apresentando mensagens de validação ao lado do campo

## prática 20 (54)

* consultando filmes com spring web thymeleaf

### Itens

*  5401 consultando um filme a partir do título e do ano de lançamento
*  5402 consultando os filmes de um mesmo ano de lançamento
*  5403 consultando todos os filmes classificados ascendentemente por título
*  5404 apresentando o total de filmes no rodapé da lista
*  5405 apresentando gráfico de barras da quantidade de filmes por ano
*  5406 apresentando os rótulos de campo com conteúdo de arquivo de propriedades (i18n)